// -*- mode:c++ -*-
#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreResourceGroupManager.h>
#include <OgreConfigFile.h>
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreVector3.h>
#include <OgreMath.h>
#include <OgreMeshManager.h>
#include <OgreWindowEventUtilities.h>

void check_config(Ogre::Root* root) {
  if (not (root->restoreConfig() || root->showConfigDialog())) {
    Ogre::LogManager::getSingleton().logMessage("Initialize::configure_ogre => " +
                                                std::string("ERROR: unable to configure Ogre"));
  }
}

void load_resources(std::string resources_file) {
  Ogre::ConfigFile cf;
  cf.load(resources_file);

  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements()) {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typeName = i->first;
      archName = i->second;
      Ogre::ResourceGroupManager::getSingleton()
        .addResourceLocation(archName, typeName, secName);
    }
  }

  Ogre::ResourceGroupManager::getSingleton()
    .initialiseAllResourceGroups();
}


Ogre::Camera* create_camera(Ogre::Root* root) {
  Ogre::Camera* camera = root->getSceneManager("SceneManager")->createCamera("Camera");

  camera->setPosition(Ogre::Vector3(0, 10, 15));
  camera->lookAt(Ogre::Vector3(0,0,0));
  camera->setNearClipDistance(5);
  camera->setFarClipDistance(10000);

  Ogre::Viewport* viewport = root->getAutoCreatedWindow()->addViewport(camera);
  viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) / Ogre::Real(viewport->getActualHeight()));

  return camera;
}

void create_light(Ogre::Root* root){
  Ogre::SceneManager* manager = root->getSceneManager("SceneManager");
  manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

  manager->setAmbientLight(Ogre::ColourValue(0.25, 0.25, 0.25));

  Ogre::Light* light = manager->createLight("MainLight");
  light->setPosition(20, 80, 50);
  light->setCastShadows(true);
}

Ogre::SceneNode* get_node_by_name(Ogre::SceneManager* manager, Ogre::String node_name="") {
  Ogre::SceneNode* node;
  if (node_name.empty())
    node = manager->getRootSceneNode();
  else
    node = manager->getSceneNode(node_name);

  return node;
}

Ogre::Entity* create_entity_and_attach(Ogre::SceneManager* manager,
                                       Ogre::String name,
                                       Ogre::String mesh,
                                       Ogre::String target_node="",
                                       bool cast_shadows=true){
  Ogre::SceneNode* node = get_node_by_name(manager, target_node);
  Ogre::Entity* entity = manager->createEntity(name, mesh);

  node->attachObject(entity);
  return entity;
}
Ogre::SceneNode* create_node(Ogre::SceneManager* manager, Ogre::String name,
                             Ogre::String parent="") {
  Ogre::SceneNode* parent_node = get_node_by_name(manager, parent);

    return parent_node->createChildSceneNode(name);
}


Ogre::SceneNode* create_node_and_entity(Ogre::SceneManager* manager, Ogre::String name,
                                        Ogre::String mesh, Ogre::String parent="") {
  Ogre::SceneNode* node = create_node(manager, name, parent);
  Ogre::Entity* entity = create_entity_and_attach(manager, name, mesh, name);
    return node;
}

class WindowManager: public Ogre::WindowEventListener{
  Ogre::RenderWindow* window_;

public:
  bool exit;

  WindowManager(Ogre::RenderWindow* window){
    window_ = window;
    exit = false;
    Ogre::WindowEventUtilities::addWindowEventListener(window, this);
  }

  bool windowClosing(Ogre::RenderWindow* window){
    Ogre::LogManager::getSingleton().logMessage("Closing window...");
    exit = true;
    return true;
  }

  void windowClosed(Ogre::RenderWindow* window) {
    Ogre::LogManager::getSingleton().logMessage("Window closed.");
    exit = true;
  }

};

int main(int argc, char *argv[])
{

  Ogre::Root* root = new Ogre::Root("config/plugins.cfg", "config/ogre.cfg", "config/ogre.log");

  check_config(root);
  Ogre::RenderWindow* window = root->initialise(true, "Titulo");
  Ogre::SceneManager* scene_manager = root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

  load_resources("config/resources.cfg");

  Ogre::Camera* camera = create_camera(root);
  create_light(root);

  Ogre::SceneNode* node = create_node_and_entity(scene_manager, "SinbadNode", "Sinbad.mesh");

  std::cout << window->isClosed()  << std::endl;
  WindowManager windowManager(window);

  while(!windowManager.exit){
    root->renderOneFrame();
    Ogre::WindowEventUtilities::messagePump();
  }

  delete node;
  delete window;
  delete scene_manager;
  delete root;

  return 0;
}
