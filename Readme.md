![ogre-logo-wetfloor.gif](https://bitbucket.org/repo/jbqkRa/images/1463996905-ogre-logo-wetfloor.gif)

# Introducción a Ogre3D                                                     

Este repositorio contiene un sencillo tutorial de Ogre3D.  
   
Aunque las explicaciones en este tutorial se han centrado en un sistema GNU/Linux, son perfectamente aplicables a un sistema Windows que tenga instalado Ogre3D.

Si lo prefiere, dispone de un pdf con la documentación en la sección de descargas de este repositorio.

Cualquier sugerencia, contactarme por: 

* ** gmail**: isaac.lacoba@gmail.com
* **twitter**: @ IsaacLacoba